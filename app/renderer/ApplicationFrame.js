/**
 * @module ApplicationFrame
 * @author tuomashatakka<tuomas.hatakka@gmail.com>
 * @flow
 */

import React, { PureComponent } from 'react'
import { ConnectedRouter } from 'connected-react-router'
import { ThemeProvider } from 'emotion-theming'
import { hot } from 'react-hot-loader'
import styled from '@emotion/styled'
import { Global, css } from '@emotion/core'

import EditorView from './views/Editor'
import PreviewView from './views/Preview'
import routes from './routes'
import theme from './styles'

const styles = (css`
  html {
    font-family:  Open Sans, Montserrat;
    font-size:    10px;
    padding:      0;
    margin:       0;
  }
  body {
    font-size:    16px;
    padding:      0;
    margin:       0;
  }
`)

const Heading = styled('h1')`
  font-family: Montserrat;
  font-size:   2em;
  color: ${p => p.theme.color};
`


class ApplicationFrame extends PureComponent {

  render () {
    return <ThemeProvider theme={ theme }>
      <Global styles={ styles } />

      <main>
        <Heading>Ableton Live 10 Theme Designer</Heading>
        <EditorView />
        <PreviewView />
      </main>

      <ConnectedRouter history={ this.props.history }>
        { routes }
      </ConnectedRouter>
    </ThemeProvider>
  }

}

export default hot(module)(ApplicationFrame)

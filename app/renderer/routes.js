import { Switch, Route } from 'react-router'
import React from 'react'

import PreviewPage from './views/Preview'


export default <Switch>
  <Route path="/" component={ PreviewPage } />
</Switch>

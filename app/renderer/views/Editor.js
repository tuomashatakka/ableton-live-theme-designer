/**
 * @module EditorView
 * @author tuomashatakka<tuomas.hatakka@gmail.com>
 * @flow
 */

import React, { PureComponent } from 'react'
import { BlockPicker } from 'react-color'


export default class EditorView extends PureComponent {

  render () {
    return <div>
      editor area
      <BlockPicker />
    </div>
  }

}

import { handleActions } from 'redux-actions'

import actions from '../actions/user'


export default handleActions(
  {
    [ actions.login ]:  login,
    [ actions.logout ]: logout,
  },
  {},
)


function login (state, action) {
  return { ...state, ...action.payload }
}

function logout (state, action) {
  return { ...state, ...action.payload }
}

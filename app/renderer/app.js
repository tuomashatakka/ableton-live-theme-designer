import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createMemoryHistory } from 'history'

import configureStore from './store'
import Application from './ApplicationFrame'


const syncHistoryWithStore = (store, history) => {
  const { router } = store.getState()
  if (router && router.location)
    history.replace(router.location)
}

const initialState  = {}
const history       = createMemoryHistory()
const store         = configureStore(initialState, history)
const rootElement   = document.querySelector(document.currentScript.getAttribute('data-container'))

syncHistoryWithStore(store, history)



render(
  <Provider store={ store }>
    <Application history={ history } />
  </Provider>,
  rootElement
)
